from divisors import get_proper_divisors


def get_triangle_number():
    count = 1
    current_triangle = 1
    while True:
        yield current_triangle
        count += 1
        current_triangle += count


def get_triangle_numbers_below(upper_limit):
    count = 1
    current_triangle = 1
    triangle_set = set()
    while current_triangle < upper_limit:
        triangle_set.add(current_triangle)
        count += 1
        current_triangle += count
    return triangle_set


def fibonacci_sequence():
    value1=1
    yield value1
    yield value1
    value2=2
    yield value2
    while True:
        new_value = value1+value2
        value1 = value2
        value2 = new_value
        yield value2


def is_number_palindrome(number):
    num_str = str(int(number))
    for i in range(0, len(num_str)):
        if num_str[i] != num_str[-(i+1)]:
            return False
    return True


def collatz_sequence(start_num):
    sequence = [start_num]
    while start_num > 1:
        if start_num % 2 == 0:
            start_num /= 2
        else:
            start_num = 3*start_num+1
        sequence.append(start_num)
    return sequence


def get_alphabetical_value(name_string):
    alphabet_string = "abcdefghijklmnopqrstuvwxyz"
    alpha_value = 0
    for letter in name_string:
        for i, v in enumerate(alphabet_string):
            if letter.lower()==v:
                alpha_value += i+1
    return alpha_value


def get_abundant_numbers():
    abundant_list = []
    for i in range(12, 28124):
        divisor_sum = sum(get_proper_divisors(i))
        if divisor_sum > i:
            abundant_list.append(i)
    return abundant_list


def get_abundant_sums():
    abundant_list = get_abundant_numbers()
    abundant_sums = set()
    abundant_list_copy = abundant_list
    for num in abundant_list:
        for num2 in abundant_list_copy:
            abundant_sums.add(num+num2)
    return abundant_sums


from itertools import permutations


def get_all_permutations_of_number(number_str):
    return [int(''.join(p)) for p in permutations(number_str)]


def get_alphabetical_position_sum(word):
    return sum(map(lambda x: ord(x.lower()) - ord('a') + 1, str(word)))


from time import time


def report_time(method):

    def report_time(*args, **kw):
        ts = time()
        result = method(*args, **kw)
        te = time()

        print('%r (result=%r) %2.2f sec' % \
              (method.__name__, result, te-ts))
        return result

    return report_time