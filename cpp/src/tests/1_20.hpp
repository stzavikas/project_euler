#ifndef SRC_TESTS_1_20_HPP_
#define SRC_TESTS_1_20_HPP_

#include <cassert>
#include <vector>
#include <numeric>
#include <functional>

#include <libs/fibonacci.hpp>
#include <libs/primes.hpp>
#include <libs/number_sets.hpp>


unsigned problem1() {
	unsigned sum = 0;
	for (unsigned i=0; i<1000; ++i) {
		if (i%3==0 or i%5==0)
			sum += i;
	}
	assert(sum==233168);
	return sum;
}

unsigned problem2() {
	auto fib_numbers = get_fibonacci_numbers_below_n(4000000);
	unsigned sum = std::accumulate(fib_numbers.begin(), fib_numbers.end(), 0,
	    [](unsigned partial_sum, unsigned num) {
			if (num%2==0)
				return partial_sum+num;
			return partial_sum;

	});
	assert(sum==4613732);
	return sum;
}

unsigned problem3() {
	std::uint64_t prime_divisor = get_max_prime_divisor(600851475143);
	assert(prime_divisor==6857);
	return prime_divisor;
}

unsigned problem4() {
	unsigned max_number = 999;
	unsigned max_palindrome = 0;
	for (unsigned i=max_number; i>=100; --i) {
		for (unsigned j=max_number; j>=100; --j) {
			unsigned prod = i*j;
			if (is_number_palindrome(prod) and prod>max_palindrome) {
				max_palindrome = prod;
			}
		}
	}
	assert(max_palindrome==906609);
	return max_palindrome;
}


#endif /* SRC_TESTS_1_20_HPP_ */
