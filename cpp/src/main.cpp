#include <iostream>
#include <vector>
#include <set>
#include <cassert>
#include <stack>
#include <stdlib.h>

#include <tests/1_20.hpp>

int main() {
	std::cout << problem1() << std::endl;
	std::cout << problem2() << std::endl;
	std::cout << problem3() << std::endl;
	std::cout << problem4() << std::endl;
//	std::cout << problem5() << std::endl;
    return 0;
}
