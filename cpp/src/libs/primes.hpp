#ifndef SRC_LIBS_PRIMES_HPP_
#define SRC_LIBS_PRIMES_HPP_

#include <vector>
#include <cmath>

std::uint64_t get_max_prime_divisor(std::uint64_t n) {
	std::uint64_t i = 2;
	while (i*i < n) {
		if (n%i==0)
			n /= i;
		i++;
	}
	return n;
}

std::set<std::uint64_t> sieve_of_eratosthenes(unsigned n) {
	std::set<std::uint64_t> primes;
	bool sieve[n] = {false};
	sieve[1] = true;
	for (std::uint64_t i=2; i<std::ceil(std::sqrt(n)); ++i) {
		if (not sieve[i]) {
			for (std::uint64_t j=i+i; j<n; j=j+i) {
				sieve[i] = true;
			}
		}
	}
	for (std::uint64_t i=0; i<n; ++i) {
		if (not sieve[i])
			primes.insert(i);
	}
	return primes;
}


#endif /* SRC_LIBS_PRIMES_HPP_ */
