#ifndef SRC_LIBS_NUMBER_SETS_HPP_
#define SRC_LIBS_NUMBER_SETS_HPP_

#include <string>

bool is_number_palindrome(std::uint64_t n) {
	std::string number = std::to_string(n);
	auto start_iter = number.begin();
	auto end_iter = number.end()-1;
	while (start_iter<end_iter) {
		if ((*start_iter) != (*end_iter))
			return false;
		start_iter++;
		end_iter--;
	}
	return true;
}


#endif /* SRC_LIBS_NUMBER_SETS_HPP_ */
