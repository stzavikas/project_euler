#ifndef SRC_LIBS_FIBONACCI_HPP_
#define SRC_LIBS_FIBONACCI_HPP_

#include <vector>
#include <cstdint>

std::vector<std::uint64_t> get_fibonacci_numbers_below_n(unsigned n) {
	std::vector<std::uint64_t> fibonacci{1, 2};
	std::uint64_t previous=1, current=2;
	while(current < n) {
		current = fibonacci.back() + previous;
		previous = fibonacci.back();
		fibonacci.push_back(current);
	}
	return fibonacci;
}



#endif /* SRC_LIBS_FIBONACCI_HPP_ */
