import math


def get_divisor_number(number):
    div_number = 2
    for i in range(2,int(math.sqrt(number)+1)):
        if number % i == 0:
            div_number += 2
    return div_number


def gcd(a, b):
    while b:
        a, b = b , a%b
    return a


def get_proper_divisors(number):
    divisors = [1]
    for i in range(2, int(int(math.sqrt(number)) + 1)):
        if number % i == 0:
            divisors.append(i)
            if i*i != number:
                divisors.append(number/i)
    return divisors

