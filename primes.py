import math


def get_prime():
    count = 2
    yield count
    count += 1
    while True:
        is_prime = True
        for i in range(2, int(math.sqrt(count) + 1)):
            if count % i == 0:
                is_prime = False
                break
        if is_prime:
            yield count
        count += 2


def is_prime(number):
    for i in range(2, int(math.sqrt(abs(number)) + 1)):
        if abs(number) % i == 0:
            return False
    return True


def get_first_n_primes(n):
    primes_buffer = set()
    for next_prime in get_prime():
        primes_buffer.add(next_prime)
        n -= 1
        if n == 0:
            return primes_buffer


def get_all_primes_smaller_than_n(n):
    sieve = [True] * n
    for i in range(3, int(math.sqrt(n))+1):
        if sieve[i]:
            for j in range(i, n, i):
                if j!=i:
                    sieve[j] = False
    return set([2] + [i for i in range(3, n, 2) if sieve[i]])


def has_substring_divisibility_property(number_str, primes):
    for i in range(1, 8):
        # Have to subtract 1 because first prime is 2
        if int(number_str[i:i+3]) % primes[i-1] != 0:
            return False
    return True