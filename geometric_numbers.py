

def get_first_n_triagonal_numbers(n):
    return get_triagonal_numbers(1, n)


def get_triagonal_numbers(start, end):
    return [int(i * (i+1) / 2) for i in range(start, end)]


def get_first_n_pentagonal_numbers(n):
    get_pentagonal_numbers(1, n)


def get_pentagonal_numbers(start, end):
    return [int(i * (3*i-1) / 2) for i in range(start, end)]


def get_first_n_hexagonal_numbers(n):
    get_hexagonal_numbers(1, n)


def get_hexagonal_numbers(start, end):
    return [int(i * (2*i-1)) for i in range(start, end)]
