from helper_functions import get_alphabetical_value, get_abundant_sums, fibonacci_sequence, report_time, \
    is_number_palindrome
from factorial import get_factorial_of_digits
from primes import get_all_primes_smaller_than_n, get_first_n_primes, is_prime
from divisors import get_proper_divisors, gcd
from digit_manipulation import has_common_digits, strip_common_digits, get_rotated_numbers, is_pandigital


@report_time
def problem21():
    divisor_sums = {}
    for i in range(1, 10000):
        divisor_sums[i] = int(sum(get_proper_divisors(i)))
    amicable_numbers = []
    for key, value in divisor_sums.items():
        if value in divisor_sums and divisor_sums[value] == key and key != value:
            amicable_numbers.append(key)
            amicable_numbers.append(value)
    result = sum(list(set(amicable_numbers)))
    assert result == 31626
    return result


@report_time
def problem22():
    name_string = ""
    with open('p022_names.txt') as myfile:
        name_string = myfile.read()
    name_list = sorted([name.strip("\"") for name in name_string.split(",")])
    alphabetical_sum = 0
    for index, value in enumerate(name_list):
        alphabetical_sum += (index+1) * get_alphabetical_value(value)
    assert alphabetical_sum == 871198282
    return alphabetical_sum


@report_time
def problem23():
    non_abundant_sum = 0
    abundant_sums = get_abundant_sums()
    for i in range(1, 28124):
        if i not in abundant_sums:
            non_abundant_sum += i
    assert non_abundant_sum == 4179871
    return non_abundant_sum


@report_time
def problem24():
    start_string = list('0123456789')
    for _ in range(1, 1000000):
        largest_k = 0
        for i in range(len(start_string)-1):
            if start_string[i] < start_string[i+1] and i > largest_k:
                largest_k = i
        largest_l = 0
        for i in range(len(start_string)):
            if largest_k < i and start_string[largest_k] < start_string[i]:
                largest_l = i
        initial_k = start_string[largest_k]
        start_string[largest_k] = start_string[largest_l]
        start_string[largest_l] = initial_k
        start_string[largest_k+1:] = reversed(start_string[largest_k+1:])
    #TODO: Assert
    print(start_string)


@report_time
def problem25():
    index = 1
    for next_value in fibonacci_sequence():
        if len(str(next_value)) >= 1000:
            break
        else:
            index += 1
    assert index == 4782
    return index


@report_time
def problem27():
    primes_buffer = get_first_n_primes(10000)
    max_prime = 0
    max_prime_coeff = (0, 0)
    for a in range(-1000, 1000):
        for b in range(-1000, 1000):
            n = 0
            while ((n*n + a*n + b) in primes_buffer):
                n += 1
            if (n > max_prime):
                max_prime = n
                max_prime_coeff = (a,b)
    result =  max_prime_coeff[0] * max_prime_coeff[1]
    assert result == -59231
    return result


@report_time
def problem28():
    matrix_size = 1002
    diagonal_sum = 1
    for n in range(3, matrix_size, 2):
        diagonal_sum += n**2
        diagonal_sum += n**2 - n + 1
        diagonal_sum += n**2 - 2*n + 2
        diagonal_sum += n**2 - 3*n + 3
    assert diagonal_sum == 669171001
    return diagonal_sum


@report_time
def problem29():
    distinct = set()
    for a in range(2, 101):
        for b in range(2, 101):
            distinct.add(a**b)
    result = len(distinct)
    assert result == 9183
    return result


@report_time
def problem30():
    fifth_powers = []
    for i in range(11, 1000000):
        current_sum = 0
        for d in str(i):
            current_sum += int(d)**5
        if current_sum == i:
            fifth_powers.append(i)
    result = sum(fifth_powers)
    assert result == 443839
    return result


@report_time
def problem32():
    pandigitals = set()
    for i in range(1, 3000):
        for j in range(i, 3000):
            product = i * j
            digits_str = str(product) + str(i) + str(j)
            if is_pandigital(digits_str):
                pandigitals.add(product)
    result = sum(pandigitals)
    assert result == 45228
    return result


@report_time
def problem33():
    result_list = []
    for i in range(11, 99):
        if i%10 == 0: continue
        for j in range(i+1, 99):
            if j%10==0: continue
            if not has_common_digits(i, j): continue
            expected_result = float(i) / float(j)
            nom, denom = strip_common_digits(i, j)
            if denom == 0 or nom == 0: continue
            if (float(nom) / float(denom)) == expected_result:
                if (gcd(nom, denom) != 1):
                    denom /= gcd(nom, denom)
                result_list.append(denom)
    result = reduce(lambda p,q: p*q, result_list)
    assert result == 200
    return result


@report_time
def problem34():
    fact = get_factorial_of_digits()
    curious_sum = 0
    for i in range(3, 100000):
        fact_sum = 0
        for d in str(i):
            fact_sum += fact[int(d)]
        if fact_sum == i:
            curious_sum += i
    assert curious_sum == 40730
    return curious_sum


@report_time
def problem35():
    primes = get_all_primes_smaller_than_n(1000000)
    circular_primes = []
    for i in range(1, 1000000):
        if '0' in str(i): continue
        if i%2==0 and i > 3: continue
        if all(int(rotated_nums) in primes for rotated_nums in get_rotated_numbers(i)):
            circular_primes.append(i)
    assert len(circular_primes) == 55
    return len(circular_primes)


@report_time
def problem36():
    palindrome_sum = 0
    for i in range(1000000):
        if is_number_palindrome(i) and is_number_palindrome(bin(i)[2:]):
            palindrome_sum += i
    assert palindrome_sum == 872187
    return palindrome_sum


def is_truncatable_prime(i, primes_list):
    i_str = str(i)
    while len(i_str)>0:
        if int(i_str) not in primes_list:
            return False
        i_str = i_str[1:]
    i_str = str(i)
    while len(i_str)>0:
        if int(i_str) not in primes_list:
            return False
        i_str = i_str[:-1]
    return True


@report_time
def problem37():
    primes = get_all_primes_smaller_than_n(1000000)
    truncatables_sum = 0
    for i in range(11, 1000000, 2):
        if '0' in str(i): continue
        if is_truncatable_prime(i, primes):
            truncatables_sum += i
    assert truncatables_sum == 748317
    return truncatables_sum


@report_time
def problem38():
    largest_pandigital = 0
    for i in range(1, 1000000):
        concatenated_product = ""
        for j in range(1, 50):
            concatenated_product += str(i*j)
            if len(concatenated_product) >= 9:
                break
        if is_pandigital(concatenated_product):
            largest_pandigital = max(largest_pandigital, int(concatenated_product))
    assert largest_pandigital == 932718654
    return largest_pandigital


@report_time
def problem39():
    keys = list(range(1, 1000))
    values = [0] * 1000
    result_perim = dict(zip(keys, values))
    for a in range(1,1000):
        b = 1
        while (a + b) < 1000:
            c = 1
            while (a + b + c) < 1000:
                if (a**2 + b**2) == c**2:
                    result_perim[(a+b+c)] += 1
                c += 1
            b += 1
    result = max(result_perim, key=result_perim.get)
    assert result==840
    return result


# problem21()
# problem22()
# problem23()
# problem24()
# problem25()
# problem27()
# problem28()
# problem29()
# problem30()
# problem32()
# problem33()
# problem34()
# problem35()
# problem36()
# problem37()
# problem39()