from geometric_numbers import get_first_n_pentagonal_numbers, get_triagonal_numbers, get_hexagonal_numbers, \
    get_pentagonal_numbers
from helper_functions import report_time, get_all_permutations_of_number, get_triangle_numbers_below, \
    get_alphabetical_position_sum
from factorial import number_of_k_combinations
from primes import is_prime, get_all_primes_smaller_than_n, has_substring_divisibility_property
from digit_manipulation import multiplied_num_contains_same_digits

@report_time
def problem41():
    max_pandigital = 0
    for i in range(9, 1, -1):
        number_str = ''
        for j in range(1, i):
            number_str += str(j)
        perm = get_all_permutations_of_number(number_str)
        perm.sort(reverse=True)
        for p in perm:
            if is_prime(int(p)):
                max_pandigital = max(int(p), max_pandigital)
                break
        if max_pandigital != 0: break
    assert max_pandigital == 7652413
    return max_pandigital


@report_time
def problem42():
    triangle_numbers = get_triangle_numbers_below(10000)
    word_string = open('p042_words.txt', 'r').read()
    words = word_string.split(",")
    words = [word.strip('"') for word in words]
    triangle_word_count = 0
    for word in words:
        if get_alphabetical_position_sum(word) in triangle_numbers:
            triangle_word_count += 1
    assert triangle_word_count==162
    return triangle_word_count


@report_time
def problem43():
    primes = list(get_all_primes_smaller_than_n(20))
    number_str = ''.join([str(i) for i in range(10)])
    permutations = get_all_permutations_of_number(number_str)
    sum_of_pandigitals = 0
    for perm in permutations:
        if has_substring_divisibility_property(str(perm), primes):
            sum_of_pandigitals += int(perm)
    assert sum_of_pandigitals == 16695334890
    return sum_of_pandigitals


@report_time
def problem44():
    d_min = None
    pentagonal = get_first_n_pentagonal_numbers(10000)
    pentagonal_set = set(pentagonal)

    class Found(Exception): pass

    try:
        for i, v in enumerate(pentagonal):
            for j in range(i+1, len(pentagonal)):
                if int(abs(pentagonal[j] - v)) in pentagonal_set and int(pentagonal[j] + v) in pentagonal_set:
                    d_min = min(d_min, int(abs(pentagonal[j] - v))) if d_min else int(abs(pentagonal[j] - v))
                    raise Found
    except Found:
        assert d_min==5482660
        return d_min
    except Exception:
        assert False


@report_time
def problem45():
    hexa = get_hexagonal_numbers(144, 100000)
    count_penta = 166
    count_tria = 286
    triangle_result = 0
    for h in hexa:
        while get_pentagonal_numbers(count_penta, count_penta+1)[0] < h:
            count_penta += 1
        if get_pentagonal_numbers(count_penta, count_penta+1)[0] != h: continue

        while get_triagonal_numbers(count_tria, count_tria+1)[0] < h:
            count_tria += 1
        if get_triagonal_numbers(count_tria, count_tria+1)[0] != h:
            continue
        else:
            triangle_result = get_triagonal_numbers(count_tria, count_tria+1)[0]
            break
    assert triangle_result == 1533776805
    return triangle_result


@report_time
def problem46():
    primes = get_all_primes_smaller_than_n(10000)
    for i in range(3, 10000, 2):
        found = False
        if i not in primes: #Is composite
            for p in primes:
                if any(i == p + (2 * (b**2)) for b in range(1, 100)):
                    found = True
                    break
            if not found:
                assert i == 5777
                return(i)


@report_time
def problem47():
    sieve = [0] * 1000000
    for i in range(2, 1000000):
        if sieve[i] == 0:
            for j in range(i, 1000000, i):
                sieve[j] += 1
    for i in range(1000000):
        if all(sieve[j]==4 for j in range(i, i+4) ):
            print(i)
            assert i==134043
            return i


@report_time
def problem48():
    sum = 0
    for i in range(1, 1000):
        sum += pow(i, i)
    print(str(sum)[-10:])
    return str(sum)[-10:]


@report_time
def problem49():
    primes = get_all_primes_smaller_than_n(10000)
    primes = set(filter(lambda x:x>1000, primes))
    for i in range(1487, 3333, 2):
        if i not in primes: continue
        for j in range(i+2, 6665, 2):
            if (j-i) > 9999-j: break
            if j not in primes : continue
            for k in range(j+2, j + j-i, 2):
                if (k-j) > (j-i) : break
                if k not in primes: continue
                if (k - j) == (j - i) and sorted(str(k)) == sorted(str(j)) and sorted(str(j)) == sorted(str(i)) and i != 1487:
                    result = str(i) + str(j) + str(k)
                    assert result=="296962999629"
                    return result


@report_time
def problem50():
    prime_set = get_all_primes_smaller_than_n(1000000)
    primes = sorted(list(prime_set))
    greatest_count = 0
    result_sum = []
    for i in range(0, len(primes)):
        prime_sum = 0
        for j in range(i, len(primes)):
            if (prime_sum > 1000000) : break
            prime_sum += primes[j]
            if (j-i)>greatest_count and prime_sum in prime_set:
                result_sum = prime_sum
                greatest_count = j-i
    assert result_sum==997651
    return result_sum


@report_time
def problem51():
    pass


@report_time
def problem52():
    for i in range(1, 100000000):
        if multiplied_num_contains_same_digits(i):
            assert i==142857
            return i


@report_time
def problem53():
    gt_million_count = 0
    for n in range(1, 101):
        for r in range(1, n+1):
            if number_of_k_combinations(n, r) >= 1000000:
                gt_million_count += 1
    assert gt_million_count==4075
    return gt_million_count


@report_time
def problem54():
    pass


# problem41()
# problem42()
# problem43()
# problem44()
# problem45()
# problem46()
# problem47()
# problem48()
# problem49()
# problem50()
problem52()
problem53()