def is_pandigital(digits_str):
    if len(digits_str) != 9:
        return False
    else:
        for d in range(1, 10):
            if str(d) not in digits_str:
                return False
    return True


def get_rotated_numbers(number):
    rotated_numbers = []
    number = str(number)
    for i in range(len(number)):
        number = number[1:] + number[0]
        rotated_numbers.append(number)
    return rotated_numbers


def has_common_digits(number1, number2):
    for d in str(number1):
        if d in str(number2):
            return True
    return False


def strip_all_digits_from_integer(number, d):
    return int(str(number).replace(d, ""))


def strip_digit_from_integer(number, d):
    return int(str(number).replace(d, "", 1))


def strip_common_digits(nom, denom):
    for d in str(nom):
        if d in str(denom):
            return strip_digit_from_integer(nom, d), strip_digit_from_integer(denom, d)


def digit_to_string(number):
    if number in ['1', '2', '6']:
        return 3
    elif number in ['4', '5', '9']:
        return 4
    elif number in ['3', '7', '8']:
        return 5
    else:
        return 0


def first_decade_to_string(number):
    if number in ['10']:
        return 3
    elif number in ['11', '12']:
        return 6
    elif number in ['15', '16']:
        return 7
    elif number in ['13', '14', '18', '19']:
        return 8
    elif number in ['17']:
        return 9


def decade_to_string(number):
    if number in ['7']:
        return 7
    elif number in ['4', '5', '6']:
        return 5
    elif number in ['2', '3', '8', '9']:
        return 6
    else:
        return 0


def multiplied_num_contains_same_digits(initial_num):
    num_str = sorted(str(initial_num))
    for i in range(2, 7):
        multiplied = initial_num * i
        if num_str != sorted(str(multiplied)):
            return False
    return True
