

def factorial(n):
    prod = 1
    for i in range(1, n+1):
        prod *= i
    return prod


def get_factorial_of_digits():
    fact = {0: 1}
    for i in range(1, 10):
        fact[i] = factorial(i)
    return fact


def number_of_k_combinations(n, k):
    if k>n:
        return 0
    return factorial(n) / (factorial(k) * factorial(n-k))
